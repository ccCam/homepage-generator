import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject
from os.path import abspath, dirname, join

WHERE_AM_I = abspath(dirname(__file__))

class Handler:
    def on_main_window_destroy(self, *args):
        Gtk.main_quit()

    def on_btn_go_clicked(self, button):
		
		#Clear
		f = open("homepage.html", "w")
		f.write("")
		f.close()

		#Open for append
		f = open("homepage.html", "a")
        
        
		#print(gui.color_to_hex(gui.color_main.get_color()))
		#Generate the start of the HTML code with the background colour
		gui.gen_start(gui.color_to_hex(gui.color_main.get_color()), f)
		
		#Generate the divs one by one for each input
		gui.gen_div(gui.name_one.get_text(), gui.image_one.get_text(), gui.url_one.get_text(), f)
		gui.gen_div(gui.name_two.get_text(), gui.image_two.get_text(), gui.url_two.get_text(), f)
		gui.gen_div(gui.name_three.get_text(), gui.image_three.get_text(), gui.url_three.get_text(), f)
		gui.gen_div(gui.name_four.get_text(), gui.image_four.get_text(), gui.url_four.get_text(), f)
		gui.gen_div(gui.name_five.get_text(), gui.image_five.get_text(), gui.url_five.get_text(), f)
		gui.gen_div(gui.name_six.get_text(), gui.image_six.get_text(), gui.url_six.get_text(), f)

		#Finish off
		gui.gen_end(f)
		
        #Close to save the file
		f.close()
        



class MainWindow(object):

	def __init__(self):
		
		#Sets up the base builder
		self.builder = Gtk.Builder()
		print(join(WHERE_AM_I, 'HomepageGenerator.glade'))
		self.builder.add_from_file(join(WHERE_AM_I, 'HomepageGenerator.glade'))
		
		#Connect all the signals
		self.builder.connect_signals(Handler())
		
		#Set the main window
		self.window = self.builder.get_object("main_window")
		
		#Connect name entries
		self.name_one = self.builder.get_object("entry_name1")
		self.name_two = self.builder.get_object("entry_name2")
		self.name_three = self.builder.get_object("entry_name3")
		self.name_four = self.builder.get_object("entry_name4")
		self.name_five = self.builder.get_object("entry_name5")
		self.name_six = self.builder.get_object("entry_name6")
		
		#Connect image entries
		self.image_one = self.builder.get_object("entry_img1")
		self.image_two = self.builder.get_object("entry_img2")
		self.image_three = self.builder.get_object("entry_img3")
		self.image_four = self.builder.get_object("entry_img4")
		self.image_five = self.builder.get_object("entry_img5")
		self.image_six = self.builder.get_object("entry_img6")
		
		#Connect URL entries
		self.url_one = self.builder.get_object("entry_url1")
		self.url_two = self.builder.get_object("entry_url2")
		self.url_three = self.builder.get_object("entry_url3")
		self.url_four = self.builder.get_object("entry_url4")
		self.url_five = self.builder.get_object("entry_url5")
		self.url_six = self.builder.get_object("entry_url6")
		
		#Connect everything else
		self.color_main = self.builder.get_object("color_main")
		
		#Show the window
		self.window.show_all()
		
	#Generates the start of the HTML file with colour
	def gen_start(self, color, f):
		print("Color: " + color)
		f.write("<html>")
		f.write("<title>Homepage</title>")
		f.write("<body bgcolor=" + color + ">")
			
	#Generate the end of the file, adding a DDG search box
	def gen_end(self, f):
		f.write("""
			<center>
			<iframe src="https://duckduckgo.com/search.html?prefill=Search DuckDuckGo" style="overflow:hidden;margin:0;padding:0;width:408px;height:40px;" frameborder="0"></iframe>
			</center>
			</body>
			</html>
		""")
		
	#Generate each div with name, image, and href link
	def gen_div(self, name, img, link, f):
		
		#Start by splitting the link to check it has http/s
		#if not, add http
		if link.split(":")[0] != "http" or link.split(":")[0] != "https":
			link = "http://" + link
		
		#Multiline with formatting
		f.write("""
		<div style="width: 30%; diplay: inline-block; float: left; margin-left: 2.5%;">
		<a href="{link}">
		<img src="{img}" style="width: 100%" />
		<h2>{name}</h2>
		</a>
		</div>
		""".format(link=link, img=img, name=name))
			
	#Change a GtkColor to a hex colour code. 
	def color_to_hex(self, color):
		#Creates a float colour then multiplies by 255 to create standard RGB
		red_float = (float(color.red) / 65535) * 255
		blue_float = (float(color.blue) / 65535) * 255
		green_float = (float(color.green) / 65535) * 255
		
		#Creates an array for the floats
		rgb = (red_float, green_float, blue_float)
		#Passes them in and does something I don't quite understand
		hex_string = '#%02x%02x%02x' % rgb
		
		return hex_string
	
			

if __name__ == '__main__':
	try:
		gui = MainWindow()
		Gtk.main()
	except KeyboardInterrupt:
		pass
	
    
